import React from 'react';
import './Keyboard.css';
import {useDispatch, useSelector} from "react-redux";
import {TRUE_PASS} from "../../constanst";

const Keyboard = () => {
    const dispatch = useDispatch();
    const passString = useSelector(state => state.passString);
    const secretString = useSelector(state => state.secretString);
    const checkResult = useSelector(state => state.checkResult);
    const removeSymbol = () => dispatch({type: 'REMOVE'});
    const addSymbol = e => {
        dispatch({type: 'ADD', payload: e.target.innerHTML});
    }
    const clearPassString = () => dispatch({type: 'CLEAR'});
    const changeCheckResult = result => {
        dispatch({type: 'CHECK_RESULT', payload: result});
    }

    let resultContent = <div>{secretString}</div>;

    if (checkResult === false) {
        resultContent = <div className="Red">Access Denied</div>
    } else if (checkResult === true) {
        resultContent = <div className="Green">Access Granted</div>
    }

    const doCheckResult = () => {
        if (passString === TRUE_PASS) {
            changeCheckResult(true);
        } else {
            changeCheckResult(false);
        }
        clearPassString();
    };

    return (
        <div className="Keyboard">
            <div className="pass_result">{resultContent}</div>
            <div className="button-block">
                <div>
                    <button onClick={e => addSymbol(e)}>7</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>8</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>9</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>4</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>5</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>6</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>1</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>2</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>3</button>
                </div>
                <div>
                    <button onClick={removeSymbol}>v</button>
                </div>
                <div>
                    <button onClick={e => addSymbol(e)}>0</button>
                </div>
                <div>
                    <button onClick={doCheckResult}>E</button>
                </div>
            </div>
        </div>
    );
};

export default Keyboard;