import React from 'react';
import Keyboard from "./containers/Keyboard/Keyboard";

const App = () => {
  return (
      <div>
        <Keyboard />
      </div>
  );
};

export default App;
