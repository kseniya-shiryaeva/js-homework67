const initialState = {
    passString: '',
    secretString: '',
    checkResult: null
};

const reducer = (state = initialState, action) => {
    if (action.type === 'ADD' && state.passString.length < 4) {
        return {...state, passString: state.passString + action.payload, secretString: state.secretString + '*'};
    }

    if (action.type === 'REMOVE') {
        return {...state, passString: state.passString.slice(0, -1), secretString: state.passString.slice(0, -1)};
    }

    if (action.type === 'CLEAR') {
        return {...state, passString: '', secretString: ''};
    }

    if (action.type === 'CHECK_RESULT') {
        return {...state, checkResult: action.payload};
    }

    return state;
}

export  default reducer;